var focusOrCreateTab = function (url, next) {
  chrome.tabs.query({
    url: url
  }, function (tabs) {
    var tab = tabs && tabs[0] ? tabs[0] : null;

    if (tab) chrome.tabs.update(tab.id, { selected: true }, next);
    else chrome.tabs.create({ url: url, selected: true }, function (tab) {
      return next(tab, true);
    });
  });
};

chrome.browserAction.onClicked.addListener(function () {
  var url = 'https://compte.laposte.net/inscription/index.do';

  focusOrCreateTab(url, function (tab, created) {
    if (created) {
      chrome.webNavigation.onCompleted.addListener(function () {
        chrome.tabs.sendMessage(tab.id, {});
      });
    } else chrome.tabs.sendMessage(tab.id, {});
  });
});