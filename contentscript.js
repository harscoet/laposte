var rand = function (min, max) {
  return Math.floor((Math.random() * max) + min);
};

var randId = function () {
  var text = '';
  var possible = 'abcdefghijklmnopqrstuvwxyz0123456789';

  for (var i=0; i < rand(10, 30); i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
};

var randName = function () {
  var names = ['Benoit', 'Martin', 'Alphonse', 'Georges', 'Maurice', 'Thomas', 'Franck', 'Fort', 'Jose', 'Patrick'];

  return names[rand(0, names.length)];
};

var randAddress = function () {
  var address = ['11 rue de la liberté', '94 Avenue du papier', '106 Boulevard Alphonse'];

  return address[rand(0, address.length)];
};

chrome.runtime.onMessage.addListener(function () {
  var password = 'Password90',
      lastname = randName(),
      firstname = randName();

  document.querySelector('form fieldset:first-child .form-group li:nth-child(2) input').value = lastname;
  document.getElementById('ipPrenom').value = firstname;
  document.querySelector('[name=ipDateNaisJour]').value = 12;
  document.querySelector('[name=ipDateNaisMois]').value = 'avril';
  document.querySelector('[name=ipDateNaisAnnee]').value = 1994;
  document.querySelector('[name=ipVoie]').innerHTML = randAddress();
  document.querySelector('[name=ipCP]').value = '75000';
  document.querySelector('[name=ipVille]').value = 'Paris';
  document.querySelector('[name=ipMail]').value = lastname.toLowerCase() + firstname.toLowerCase() + randId();
  document.querySelector('[name=ipPwd]').value = password;
  document.querySelector('[name=ipPwd2]').value = password;
  document.querySelector('[name=ipQuest]').value = 6;
  document.querySelector('[name=ipRep]').value = 'Superman';
  document.querySelector('[name=conditionscbx]').checked = true;
  document.querySelector('[name=jcaptcha_response]').focus();
});